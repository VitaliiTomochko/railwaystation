package com.epam.scheduler;

import com.epam.domain.Station;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ScheduleItem extends Comparable<ScheduleItem> {

    Station getStation();

    /**
     * This is time on give date, when train is supposed to leave station returned by {@link #getStation()}.
     */
    LocalDateTime getDepartureTime();

    /**
     * TrainMetadata arrival time to station
     */

    LocalDateTime getArrivalTime();

    LocalDate getDate();

    default LocalDateTime moveToNewDate(LocalDateTime from, LocalDate to) {
        return LocalDateTime.of(to.getYear(), to.getMonthValue(), to.getDayOfMonth(), from.getHour(), from.getMinute());
    }
}
