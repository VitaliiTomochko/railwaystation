package com.epam.scheduler;

import com.epam.domain.Station;
import com.epam.domain.TrainMetadata;

import java.time.LocalDate;
import java.util.SortedSet;

/**
 * This interface is kind of {@link java.util.function.Predicate};<br/>
 * <b>ScheduleProvider</b> tells if train available on date.
 * <p>
 * This interface assumes that both consumer of this interface and {@link TrainMetadata}
 * and {@link Station} operate in the same time zone.
 * <p>
 * <br/>
 * <br/>
 * <b>Note:<b/> TimeZone is not supported so if train is traveling across many regions of
 * different timezones(for example Ukraine-Poland, or Ukraine-Russia) the classes that
 * implement this interface will behave unpredictably.
 * <p>
 * In order to support timezones correctly:
 * <li>all date and time calculations have to be done
 * * in UTC timezone. But this drastically makes debugging a nightmare.</li>
 * <li>All the methods in this calls would have to accept timezone of runtime
 * consumer or origin station of travel<li/>
 */
public interface ScheduleProvider {
    /**
     * Tells if there is train scheduled on date
     *
     * @param localDate Date. without timezone
     * @return
     */
    boolean isTrainAvailableOnDate(LocalDate localDate);

    /**
     * There could be many trains from {@link Station} on the give date {@link LocalDate}.
     * <p>
     * If this is the case then {@link #isTrainAvailableOnDate(LocalDate)} will return true on given date
     * then this method returns list of available trains on that date.
     *
     * @param localDate
     * @return list of schedules.
     */
    SortedSet<ScheduleItem> getSchedulesOnDate(LocalDate localDate);
}
