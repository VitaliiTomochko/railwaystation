package com.epam.scheduler.impl;

import com.epam.domain.Station;
import com.epam.scheduler.ScheduleItem;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class DefaultScheduleItem implements ScheduleItem {

    private final LocalDateTime departureTime;
    private final LocalDateTime arrivalTime;
    private final LocalDate date;
    private final Station station;

    public DefaultScheduleItem(final Station station,
                               final LocalDateTime arrivalTime,
                               final LocalDateTime departureTime) {
        this.arrivalTime = Objects.requireNonNull(arrivalTime);
        this.departureTime = Objects.requireNonNull(departureTime);
        this.date = departureTime.toLocalDate();
        this.station = Objects.requireNonNull(station);
    }

    @Override
    public Station getStation() {
        return station;
    }

    @Override
    public int compareTo(final ScheduleItem o) {
        return this.departureTime.compareTo(o.getDepartureTime());
    }

    @Override
    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    @Override
    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    @Override
    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("%s:%s-%s", station, departureTime.getHour(), departureTime.getMinute());
    }
}
