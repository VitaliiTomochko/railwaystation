package com.epam.scheduler.impl;

import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.ScheduleProvider;

import java.time.LocalDate;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Schedule Provider for even days, semantically means all days for which this predicate id true: n % 2 == 0
 */
public class EvenDayScheduleProvider implements ScheduleProvider {
    protected final SortedSet<ScheduleItem> scheduleItems;

    public EvenDayScheduleProvider(final SortedSet<ScheduleItem> scheduleItems) {
        this.scheduleItems = scheduleItems;
    }

    @Override
    public boolean isTrainAvailableOnDate(LocalDate localDate) {
        return localDate.getDayOfMonth() % 2 == 0;
    }

    @Override
    public SortedSet<ScheduleItem> getSchedulesOnDate(LocalDate localDate) {
        if (isTrainAvailableOnDate(localDate)) {
            //need to augment all scheduled times to the given localDate
            return scheduleItems.stream()
                .map(e -> new DefaultScheduleItem(e.getStation(),
                    e.moveToNewDate(e.getArrivalTime(), localDate),
                    e.moveToNewDate(e.getDepartureTime(), localDate))
                ).collect(Collectors.toCollection(TreeSet::new));

        } else {
            return Collections.emptySortedSet();
        }
    }
}
