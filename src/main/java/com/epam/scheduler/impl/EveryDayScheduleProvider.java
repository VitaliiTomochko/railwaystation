package com.epam.scheduler.impl;

import com.epam.scheduler.ScheduleItem;

import java.time.LocalDate;
import java.util.SortedSet;

public class EveryDayScheduleProvider extends EvenDayScheduleProvider {
    public EveryDayScheduleProvider(final SortedSet<ScheduleItem> scheduleItems) {
        super(scheduleItems);
    }

    @Override
    public boolean isTrainAvailableOnDate(final LocalDate localDate) {
        return true;
    }
}
