package com.epam.scheduler.impl;

import com.epam.scheduler.ScheduleItem;

import java.time.LocalDate;
import java.util.SortedSet;


public class OddDayScheduleProvider extends EvenDayScheduleProvider {

    public OddDayScheduleProvider(final SortedSet<ScheduleItem> scheduleItems) {
        super(scheduleItems);
    }

    @Override
    public boolean isTrainAvailableOnDate(final LocalDate localDate) {
        return !super.isTrainAvailableOnDate(localDate);
    }

}
