package com.epam.travel;

import com.epam.domain.Station;


public interface TravelIntent {
    Station getFrom();

    Station getTo();
}
