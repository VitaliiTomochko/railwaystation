package com.epam.util;

import com.epam.domain.Station;
import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.impl.DefaultScheduleItem;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

//static methods that do miscellaneous crap
public class Utils {
    private static final Comparator<Map.Entry<Station, ScheduleItem>> ENTRY_COMPARATOR = Comparator.comparing(Map.Entry::getValue);

    //returns string like Lviv:11-05 -> Kyiv:12:10
    public static String mapToHuman(Map<Station, ScheduleItem> stationScheduleItemMap) {
        final SortedSet<ScheduleItem> scheduleItems = new TreeSet<>(stationScheduleItemMap.values());
        return mapToHuman(scheduleItems);
    }

    public static String mapToHuman(SortedSet<ScheduleItem> scheduleItems) {

        final Iterator<CharSequence> iterator = new Iterator<CharSequence>() {
            final Iterator<ScheduleItem> iterator = scheduleItems.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public CharSequence next() {
                return iterator.next().toString();
            }
        };

        final Iterable<? extends CharSequence> elements = (Iterable<CharSequence>) () -> iterator;

        final String r = String.join(" -> ", elements);
        return r;
    }

    public static ScheduleItem buildScheduleItem(final Station station, final LocalDateTime departureDateTime) {
        //lets assume arrival time is 10 minutes before departure time.
        LocalDateTime arrivalDateTime = departureDateTime.minusMinutes(10);
        return new DefaultScheduleItem(station, arrivalDateTime, departureDateTime);
    }

    public static LocalDateTime buildLocalDateTime(final int hour, final int minute) {
        //these do not matter:
        final int year = 2020;
        final int month = 1;
        final int dayOfMonth = 1;

        return LocalDateTime.of(year, month, dayOfMonth, hour, minute);
    }
}
