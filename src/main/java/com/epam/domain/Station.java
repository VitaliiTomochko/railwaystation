package com.epam.domain;

import java.time.LocalDate;
import java.util.Set;

/**
 * Defines a station, like Lviv main railway station, Kiev-Pass I or Kyiv-Pass II
 */
public interface Station {
    String getName();

    //once Trains are configured they register themselves that they go through this station.
    Set<TrainMetadata> getTrainMetadatas(LocalDate localDate);

    Set<TrainMetadata> getAllTrainMetadatas();

    void addTrainMetadata(TrainMetadata trainMetadata);
}
