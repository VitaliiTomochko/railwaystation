package com.epam.domain;

import java.util.Set;

public interface Car {
    Set<Seat> getSeats();

    String getCarIdentifier();
}
