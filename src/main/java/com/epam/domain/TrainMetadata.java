package com.epam.domain;

import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.ScheduleProvider;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

/**
 * TrainMetadata describes route details, like list of stations, train goes through, when it goes through those stations,
 * car and seat configuration.
 * <p>
 * This class is blue print for TrainRun
 */
public interface TrainMetadata {
    /**
     * Gets stations this train comes through and time when train leaves this station
     *
     * @return
     */
    Map<Station, ScheduleItem> getScheduleMap();

    ScheduleProvider getScheduleProvider();

    Set<Car> getCars();

    /**
     * Prefix for train.
     * for example Lviv-Kyiv is 92,
     * (translating this to airlines it would be "PS 34" from Lviv to Kyiv)
     *
     * @return
     */
    String getTrainIdentifier();

    /**
     * @return returns LocalDateTime Departure
     */
    LocalDateTime getDepartureLocalDateTime();

    /**
     * Same as {@link #getDepartureLocalDateTime()}
     *
     * @return
     */
    String getDepartureLocalDateTimeAsString();

    public Station getStartStation();

    public Station getEndStation();

    //uniq id for db
    String getId();
}