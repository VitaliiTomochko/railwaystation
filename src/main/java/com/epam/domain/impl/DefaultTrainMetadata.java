package com.epam.domain.impl;

import com.epam.domain.Car;
import com.epam.domain.Station;
import com.epam.domain.TrainMetadata;
import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.ScheduleProvider;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class DefaultTrainMetadata implements TrainMetadata {

    private final String trainIdentifier;
    private final Map<Station, ScheduleItem> stationScheduleItemMap;
    private final ScheduleProvider scheduleProvider;
    private final Set<Car> carList;
    private final String id;

    //departure from original station
    private final LocalDateTime departureLocalDateTime;
    private final Station startStation;
    private final Station endStation;


    public DefaultTrainMetadata(final String trainIdentifier,
                                final Map<Station, ScheduleItem> stationScheduleItemMap,
                                final ScheduleProvider scheduleProvider,
                                final Set<Car> carList) {
        if (stationScheduleItemMap.entrySet().isEmpty() || stationScheduleItemMap.entrySet().size() < 2) {
            throw new IllegalArgumentException("stationScheduleItemMap is empty, or does not contain end station");
        }

        this.stationScheduleItemMap = stationScheduleItemMap;
        this.scheduleProvider = scheduleProvider;
        this.carList = carList;
        this.trainIdentifier = trainIdentifier;
        this.departureLocalDateTime = stationScheduleItemMap.values().stream().sorted().findFirst().get().getDepartureTime();
        this.startStation = stationScheduleItemMap.values().stream().sorted().findFirst().get().getStation();
        this.endStation = stationScheduleItemMap.values().stream().sorted().skip(stationScheduleItemMap.entrySet().size() - 1).findFirst().get().getStation();

        stationScheduleItemMap.entrySet()
            .forEach(stationScheduleItemEntry ->
                stationScheduleItemEntry.getKey().addTrainMetadata(this));
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public Map<Station, ScheduleItem> getScheduleMap() {
        return stationScheduleItemMap;
    }

    @Override
    public ScheduleProvider getScheduleProvider() {
        return scheduleProvider;
    }

    @Override
    public Set<Car> getCars() {
        return carList;
    }

    @Override
    public String getTrainIdentifier() {
        return trainIdentifier;
    }

    @Override
    public LocalDateTime getDepartureLocalDateTime() {
        return departureLocalDateTime;
    }

    @Override
    public String getDepartureLocalDateTimeAsString() {
        return departureLocalDateTime.format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    @Override
    public Station getStartStation() {
        return startStation;
    }

    @Override
    public Station getEndStation() {
        return endStation;
    }

    @Override
    public String getId() {
        return id;
    }
}
