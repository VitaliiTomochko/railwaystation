package com.epam.domain.impl;

import com.epam.domain.Station;
import com.epam.domain.TrainMetadata;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Defines a station, like Lviv main railway station, Kiev-Pass I or Kyiv-Pass II
 */
public class DefaultStation implements Station {
    private final Set<TrainMetadata> trainMetadataHashSet = new HashSet<>();
    private final String name;

    //Station was born
    public DefaultStation(final String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<TrainMetadata> getTrainMetadatas(final LocalDate localDate) {
        return trainMetadataHashSet.stream()
            .filter(trainMetadata -> trainMetadata.getScheduleProvider().isTrainAvailableOnDate(localDate))
            .collect(Collectors.toSet());
    }

    @Override
    public Set<TrainMetadata> getAllTrainMetadatas() {
        return trainMetadataHashSet;
    }

    @Override
    public String toString() {
        return "Station: \'" + name + '\'';
    }

    @Override
    public void addTrainMetadata(TrainMetadata trainMetadata) {
        trainMetadataHashSet.add(trainMetadata);
    }
}
