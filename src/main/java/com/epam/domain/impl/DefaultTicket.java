package com.epam.domain.impl;

import com.epam.domain.Passenger;
import com.epam.domain.Ticket;

import java.math.BigDecimal;

public class DefaultTicket implements Ticket {

    private final Passenger passenger;
    private final BigDecimal price;
    private final TrainRun trainRun;

    public DefaultTicket(final Passenger passenger,
                         final BigDecimal price,
                         final TrainRun trainRun) {
        this.passenger = passenger;
        this.price = price;
        this.trainRun = trainRun;
    }

    @Override
    public TrainRun getTrainRun() {
        return this.trainRun;
    }

    @Override
    public BigDecimal getPrice() {
        return this.price;
    }

    @Override
    public Passenger getPassenger() {
        return this.passenger;
    }
}
