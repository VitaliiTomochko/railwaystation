package com.epam.domain.impl;


import com.epam.domain.Car;
import com.epam.domain.Station;
import com.epam.domain.Ticket;
import com.epam.domain.TrainMetadata;
import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.ScheduleProvider;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Train Run is either a scheduled event to occur in the future or train that is already enroute to it's destination.
 */
public class TrainRun implements TrainMetadata {
    private final TrainMetadata trainMetadata;
    private final Set<Ticket> tickets;
    private final String id;
    //Date and Time when train run is suppose to leave start station
    private final LocalDateTime scheduledDeparture;

    /**
     * Private constructor! Use {@link #supplementFromDb(TrainMetadata, LocalDateTime)} to build a new instance of this class.
     *
     * @param trainMetadata
     * @param tickets
     */
    private TrainRun(final TrainMetadata trainMetadata,
                     final LocalDateTime scheduledDeparture,
                     final Set<Ticket> tickets) {
        this.trainMetadata = trainMetadata;
        this.tickets = tickets;
        this.id = UUID.randomUUID().toString();
        this.scheduledDeparture = scheduledDeparture;
    }

    private static Optional<TrainRun> loadFromDB(final TrainMetadata trainMetadata) {
        //TODO use either jdbc or hibernate to load it from db
        return Optional.empty();
    }

    /**
     * Load train runt data from persistence layer. Called when we need to figure out already sold tickets from DB.
     *
     * @return if no train run was found in persistence layer, a new {@link TrainRun} is constructed.
     */

    public static TrainRun supplementFromDb(final TrainMetadata trainMetadata, final LocalDateTime scheduledDepartureDateTime) {
        final LocalDate depatureDate = scheduledDepartureDateTime.toLocalDate();
        //lets check if departure date is valid:
        if (trainMetadata.getScheduleProvider().isTrainAvailableOnDate(depatureDate)) {
            final long count = trainMetadata.getScheduleProvider()
                .getSchedulesOnDate(depatureDate)
                .stream()
                .map(scheduleItem ->
                    scheduleItem.moveToNewDate(scheduleItem.getDepartureTime(), depatureDate))
                .filter(localDateTime -> localDateTime.equals(scheduledDepartureDateTime)).count();
            if (count > 0 && count < 2) {
                return loadFromDB(trainMetadata).orElseGet(() -> new TrainRun(trainMetadata, scheduledDepartureDateTime, Collections.emptySet()));
            }
        }
        throw new IllegalArgumentException("Train is not scheduled to leave at " + scheduledDepartureDateTime);
    }

    @Override
    public Map<Station, ScheduleItem> getScheduleMap() {
        return trainMetadata.getScheduleMap();
    }

    @Override
    public ScheduleProvider getScheduleProvider() {
        return trainMetadata.getScheduleProvider();
    }

    @Override
    public Set<Car> getCars() {
        return trainMetadata.getCars();
    }

    @Override
    public String getTrainIdentifier() {
        return trainMetadata.getTrainIdentifier();
    }

    @Override
    public LocalDateTime getDepartureLocalDateTime() {
        return trainMetadata.getDepartureLocalDateTime();
    }

    @Override
    public String getDepartureLocalDateTimeAsString() {
        return trainMetadata.getDepartureLocalDateTimeAsString();
    }

    @Override
    public Station getStartStation() {
        return this.trainMetadata.getStartStation();
    }

    @Override
    public Station getEndStation() {
        return this.trainMetadata.getEndStation();
    }

    @Override
    public String getId() {
        return id;
    }
}
