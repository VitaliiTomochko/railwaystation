package com.epam.domain.impl;

import com.epam.domain.Seat;

public class DefaultSeat implements Seat {
    private final String seatIdentifier;

    public DefaultSeat(String seatIdentifier) {
        this.seatIdentifier = seatIdentifier;
    }
}
