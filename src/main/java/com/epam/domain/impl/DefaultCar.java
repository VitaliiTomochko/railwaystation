package com.epam.domain.impl;

import com.epam.domain.Car;
import com.epam.domain.Seat;

import java.util.Set;

public class DefaultCar implements Car {

    private final Set<Seat> seats;
    private final String carIdentifier;

    public DefaultCar(final String carIdentifier, final Set<Seat> seats) {
        this.seats = seats;
        this.carIdentifier = carIdentifier;
    }

    @Override
    public Set<Seat> getSeats() {
        return seats;
    }

    @Override
    public String getCarIdentifier() {
        return carIdentifier;
    }
}
