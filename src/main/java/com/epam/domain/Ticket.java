package com.epam.domain;

import com.epam.domain.impl.TrainRun;

import java.math.BigDecimal;


public interface Ticket {
    TrainRun getTrainRun();

    BigDecimal getPrice();

    Passenger getPassenger();
}
