package com.epam.domain;


public class Passenger {
    final String name;
    final String surname;

    public Passenger(final String name, final String surname) {
        this.name = name;
        this.surname = surname;
    }
}
