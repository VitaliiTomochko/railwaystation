package com.epam;


import com.epam.domain.Car;
import com.epam.domain.Seat;
import com.epam.domain.Station;
import com.epam.domain.TrainMetadata;
import com.epam.domain.impl.DefaultCar;
import com.epam.domain.impl.DefaultSeat;
import com.epam.domain.impl.DefaultStation;
import com.epam.domain.impl.DefaultTrainMetadata;
import com.epam.domain.impl.TrainRun;
import com.epam.scheduler.ScheduleItem;
import com.epam.scheduler.ScheduleProvider;
import com.epam.scheduler.impl.EvenDayScheduleProvider;
import com.epam.scheduler.impl.OddDayScheduleProvider;
import com.epam.util.Utils;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Unit test for simple App.
 */
public class TrainTest {
    private final Set<Seat> seats = Stream.of("#10", "#12", "#13", "#14", "#21", "#22").map(DefaultSeat::new).collect(Collectors.toSet());
    private final Set<Car> carList = Stream.of("1", "2", "3", "4", "5", "6").map(s -> new DefaultCar(s, seats)).collect(Collectors.toSet());

    private Station lviv;
    private LocalDateTime lvivDeparture;
    private Station lutsk;
    private LocalDateTime lutskDeparture;
    private Station rivno;
    private LocalDateTime rivnoDeparture;
    private Station zhytomyr;
    private LocalDateTime zhytomyrDeparture;
    private Station kyiv;
    private LocalDateTime kyivDeparture;
    private SortedSet<ScheduleItem> scheduleItems;
    private Map<Station, ScheduleItem> stationScheduleItemMap;

    @Before
    public void setUp() throws Exception {
        //Stations
        lviv = new DefaultStation("lviv");
        lutsk = new DefaultStation("lutsk");
        rivno = new DefaultStation("rivno");
        zhytomyr = new DefaultStation("zhytomyr");
        kyiv = new DefaultStation("kyiv");

        //Departures
        lvivDeparture = Utils.buildLocalDateTime(9, 1);
        lutskDeparture = Utils.buildLocalDateTime(11, 0);
        rivnoDeparture = Utils.buildLocalDateTime(13, 0);
        zhytomyrDeparture = Utils.buildLocalDateTime(15, 0);
        kyivDeparture = Utils.buildLocalDateTime(17, 0);

        scheduleItems = Stream.of(Utils.buildScheduleItem(lviv, lvivDeparture),
            Utils.buildScheduleItem(zhytomyr, zhytomyrDeparture),
            Utils.buildScheduleItem(rivno, rivnoDeparture),
            Utils.buildScheduleItem(lutsk, lutskDeparture),
            Utils.buildScheduleItem(kyiv, kyivDeparture))
            .collect(Collectors.toCollection(TreeSet::new));

        stationScheduleItemMap = scheduleItems.stream().collect(Collectors.toMap(ScheduleItem::getStation, e -> e));
    }

    @Test
    public void testOddDayScheduleProvider() {
        final ScheduleProvider scheduleProvider = new OddDayScheduleProvider(scheduleItems);
        assertTrue(scheduleProvider.isTrainAvailableOnDate(LocalDate.of(2020, 1, 1)));
        assertFalse(scheduleProvider.isTrainAvailableOnDate(LocalDate.of(2020, 1, 2)));
        final LocalDate d = LocalDate.of(2022, 05, 1);
        final SortedSet<ScheduleItem> dd = scheduleProvider.getSchedulesOnDate(d);
        final String r = Utils.mapToHuman(dd);
        assertEquals("Station: 'lviv':9-1 -> Station: 'lutsk':11-0 -> Station: 'rivno':13-0 -> Station: 'zhytomyr':15-0 -> Station: 'kyiv':17-0", r);
    }

    @Test
    public void testEvenDayScheduleProvider() {
        final ScheduleProvider scheduleProvider = new EvenDayScheduleProvider(scheduleItems);
        assertTrue(scheduleProvider.isTrainAvailableOnDate(LocalDate.of(2020, 1, 2)));
        assertFalse(scheduleProvider.isTrainAvailableOnDate(LocalDate.of(2020, 1, 1)));
        final LocalDate d = LocalDate.of(2022, 05, 2);
        final SortedSet<ScheduleItem> dd = scheduleProvider.getSchedulesOnDate(d);
        final String r = Utils.mapToHuman(dd);
        assertEquals("Station: 'lviv':9-1 -> Station: 'lutsk':11-0 -> Station: 'rivno':13-0 -> Station: 'zhytomyr':15-0 -> Station: 'kyiv':17-0", r);
    }


    @Test
    public void testApp() {
        final String trainIdentifier = "92";
        final Map<Station, ScheduleItem> stationScheduleItemMap = this.stationScheduleItemMap;
        final Set<Car> carList = this.carList;
        final ScheduleProvider evenDayScheduleProvider = new EvenDayScheduleProvider(scheduleItems);
        //Lets make sure before creating TrainMetadata all stations don't know what trains are going through them
        assertEquals(0, lviv.getAllTrainMetadatas().size());

        final TrainMetadata trainMetadata = new DefaultTrainMetadata(trainIdentifier, stationScheduleItemMap,
            evenDayScheduleProvider, carList);
        assertEquals(1, lviv.getAllTrainMetadatas().size());
        //for even schedule provider there must be a route on even day e.g 2025-03-22
        assertEquals(1, lviv.getTrainMetadatas(LocalDate.of(2025, 3, 22)).size());
        //and for odd day there is no route
        assertEquals(0, lviv.getTrainMetadatas(LocalDate.of(2025, 3, 21)).size());

        // Now lets create first train
        try {
            TrainRun trainRun = TrainRun.supplementFromDb(trainMetadata, LocalDateTime.of(2025, 3, 22, 9, 0));
            //this line above has to throw exception ^^^ becase there is no  trainmetadata configured for 9:00
            fail();
        } catch (Exception e) {
            //awesome, train run was not created due to validation check
        }
        //now lets create train for valid date
        TrainRun trainRun = TrainRun.supplementFromDb(trainMetadata, LocalDateTime.of(2025, 3, 22, 9, 1));
        assertNotNull(trainRun);
    }

}
